(ns day13.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.edn :as edn]))

;; input

(defn indexed [xs]
  (map-indexed vector xs))


(defn read-input [filename]
  (for [section (str/split (slurp (io/resource filename)) #"\n\n")]
    (let [[e1 e2] (str/split-lines section)]
      [(edn/read-string e1) (edn/read-string e2)])))

(def sample1 (read-input "day13/sample1.txt"))
(def full-input (read-input "day13/input13.txt"))

;; solutions

(defn compare1 [e1 e2]
  (cond
    (and (number? e1) (number? e2))
    (cond
      (< e1 e2) -1
      (> e1 e2) 1)

    (number? e1)
    (compare1 [e1] e2)

    (number? e2)
    (compare1 e1 [e2])

    (and (empty? e1) (empty? e2)) nil
    (empty? e1) -1
    (empty? e2) 1

    :else
    (if-let [res (compare1 (first e1) (first e2))]
      res
      (compare1 (subvec e1 1) (subvec e2 1)))))

(defn indexed [xs]
  (map vector (drop 1 (range)) xs))

(defn part1 [input]
  (apply +
         (for [[n [e1 e2 ]] (indexed input)
               :when (= -1 (compare1 e1 e2))]
           n)))


(defn part2 [input]
  (let [div1 [[2]]
        div2 [[6]]]
    (apply *
           (for [[n v] (indexed (sort compare1 (into (apply concat input)
                                                     [div1 div2])))
                 :when (or (= div1 v) (= div2 v))]
             n))))

(comment
  (part1 sample1)
  13
  (part1 full-input)
  6415

  (part2 sample1)
  140
  (part2 full-input)
  20056






 )

