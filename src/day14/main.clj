(ns day14.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))
(def ROCK :#)
(def SAND :o)

(def origin [500 0])
;; input


(defn points [[x1 y1] [x2 y2]]
  (cond
    (= x1 x2)
    (for [y (range (min y1 y2) (inc (max y1 y2)))]
      [x1 y])

    (= y1 y2)
    (for [x (range (min x1 x2) (inc (max x1 x2)))]
      [x y1])

    :else
    (throw (ex-info {} "invalid line" ))))

(defn add-segment [state [from to]]
  (reduce #(assoc %1 %2 :ROCK) state (points from to)))

(defn add-line [state line]
  (reduce add-segment state (partition 2 1 line)))

(defn make-map [lines]
  (reduce add-line {} lines))

(defn parse-pair [pair]
  (vec (map parse-long (str/split pair #","))))

(defn parse-line [line]
  (map parse-pair (str/split line #" -> ")))

(defn read-input [filename]
  (make-map
   (for [line (str/split-lines (slurp (io/resource filename)))]
     (parse-line line))))

(def sample1 (read-input "day14/sample1.txt"))
(def full-input (read-input "day14/input14.txt"))


;; solutions


(defn apply-delta [[x y] [dx dy]]
  [(+ x dx) (+ y dy)])

(defn drop1 [state [x y :as xy]]
  (when (< y 200) ;; arbitrary limit - meh
    (let [next-xy
          (first
           (for [delta [[0 1] [-1 1] [1 1]]
                 :let [target (apply-delta xy delta)]
                 :when (not (state target))]
             target))]
      (if next-xy
        (drop1  state next-xy)
        xy))))


(defn part1 [input]
  (loop [state input
         drops 0]
    (if-let [resting-point (drop1 state origin)]
      (recur (assoc state resting-point :sand) (inc drops))
      drops)))


(defn drop2 [state [x y :as xy] floor]
  (let [next-xy
        (first
         (for [delta [[0 1] [-1 1] [1 1]]
               :let [target (apply-delta xy delta)]
               :when (and (not (state target))
                          (< (second target) floor))]
           target))]
    (if next-xy
      (drop2 state next-xy floor)
      xy)))

(defn part2 [input]
  (let [floor (+ 2 (reduce max (map second (keys input))))]
    (loop [state input
           drops 0]
      (let [resting-point (drop2 state origin floor)]
        (if (= origin resting-point)
          (inc drops)
          (recur (assoc state resting-point :sand) (inc drops)))))))

(comment
  (part1 sample1)
  24
  (part1 full-input)
  614

  (part2 sample1)
  93

  (part2 full-input)
  26170
 )

