(ns day05.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))
;; input
(defn parse-state [lines]
  ;; sloppy parsing just to pass my input
  (let [padding "                                        "]
    (apply merge-with concat
           (for [line (reverse lines)]
             (zipmap (range 1 10)
                     (for [[_ c] (partition 4 (str line padding))]
                       (when (not= \space c) [c])))))))

(defn parse-moves [lines]
  (for [line lines
        :let [[_ x y z] (re-matches #"move (\d+) from (\d) to (\d)" line)]]
    {:n (parse-long x)
     :from (parse-long y)
     :to (parse-long z)}))

(defn read-input [filename]
  (let [[p1 p2]
        (str/split (slurp (io/resource filename)) #"\n\n")]
    {:state (parse-state (butlast (str/split-lines p1)))
     :moves (parse-moves (str/split-lines p2))}))

(def full-input (read-input "day05/input05.txt"))

;; solutions

(defn extract-answer [state]
  (apply str
         (for [n (range 1 10)]
           (last (state n)))))

(defn apply-move [state {:keys [n from to]}]
  (-> state
      (update from #(drop-last n %))
      (update to #(concat % (take n (reverse (get state from)))))))

(defn apply-move2 [state {:keys [n from to]}]
  (-> state
      (update from #(drop-last n %))
      (update to #(concat % (reverse (take n (reverse (get state from))))))))

(defn part1 [input]
  (extract-answer (reduce apply-move (:state input) (:moves input))))

(defn part2 [input]
  (extract-answer (reduce apply-move2 (:state input) (:moves input))))

(comment
  (part1 full-input)
  "ZWHVFWQWW"

  (part2 full-input)
  "HZFZCCWWV"
)


