(ns day19.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io])
  (:import [clojure.lang PersistentQueue]))

;; This is really a terrible effort. The code is very slow - basically I'm searching the space with some really
;; sketchy pruning out of states when geodes and obsidian can be made. But I'm doing the pruning kind of late.
;; It's a mess... I started rewriting while waiting for part1 to finish, anticipating I'd need to do something
;; better for part2, should my solution work at all. It turns out it did and that part2 required almost no
;; changes.

;; input

(defn read-input [filename]
  (into []
        (for [line (str/split-lines (slurp (io/resource filename)))]
          (let [[_ n a b c1 c2 d1 d2]
                (re-matches #"Blueprint (\d+): Each ore robot costs (\d+) ore. Each clay robot costs (\d+) ore. Each obsidian robot costs (\d+) ore and (\d+) clay. Each geode robot costs (\d+) ore and (\d+) obsidian." line)]
            {:id (parse-long n)
             :rules {:ore {:ore (parse-long a)}
                     :clay {:ore (parse-long b)}
                     :obsidian {:ore (parse-long c1)
                                :clay (parse-long c2)}
                     :geode {:ore (parse-long d1)
                             :obsidian (parse-long d2)}}}))))

(def sample1 (read-input "day19/sample1.txt"))
(def full-input (read-input "day19/input19.txt"))

;; part1

(def EMPTY
  {:robots {:ore 0 :clay 0 :obsidian 0 :geode 0}
   :resources {:ore 0 :clay 0 :obsidian 0 :geode 0}})


(defn can-build? [state [bot-type needed]]
  (let [new-res (merge-with - (:resources state) needed)]
    (not (some neg? (vals new-res)))))

(defn build [state [bot-type needed]]
  (let [new-res (merge-with - (:resources state) needed)]
    (if (some neg? (vals new-res))
      state
      (-> state
          (assoc :resources new-res)
          (update-in [:robots bot-type] inc)))))


(defn run-bp [bp nminutes]
  (loop [minute 0
         states #{(update-in EMPTY [:robots :ore] inc)}
         seen #{}]
    ;;(Thread/sleep 1)
    (let [max-g (apply max 0 (map #(get-in % [:resources :geode]) states))
          max-o (apply max 0 (map #(get-in % [:resources :obsidian]) states))]
      (if (= minute nminutes)
        max-g
        (do
          ;;(println "-" minute max-g max-o "::" (count states))
          (recur (inc minute)
                 (set
                  (for [state states
                        rule (conj (:rules bp) nil)
                        :let [next-state (cond-> state
                                           rule (build rule)
                                           true (update :resources #(merge-with + % (:robots state))))]
                        :when (and (not (seen next-state))
                                   (or (> (get-in next-state [:resources :geode]) max-g)
                                       (and (=  (get-in next-state [:resources :geode]) max-g)
                                            (>= (get-in next-state [:resources :obsidian] max-o)))))]
                    next-state))
                 (into seen states)))))))


(defn part1 [input]
  (reduce +
          (for [bp input
                :let [_ (println "======" bp)
                      geodes (run-bp bp 24)]]
            (* (:id bp) geodes))))

(defn part2 [input]
  (for [bp (take 3 input)
        :let [_ (println "======" bp)
              geodes (run-bp bp 32)]]
    geodes))

(comment
  (part1 full-input)

  (part2 sample1))



