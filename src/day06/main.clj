(ns day06.main
    (:require [clojure.string :as str]
            [clojure.java.io :as io]))

;; read input

(defn read-input [filename]
  (slurp (io/resource filename)))

(def full-input (read-input "day06/input06.txt"))

;; solutions

(defn solve [input packet-size]
  (first
   (for [[n word] (map-indexed vector (partition packet-size 1 input))
         :when (apply distinct? word)]
     (+ n packet-size))))

(defn part1 [input]
  (solve input 4))

(defn part2 [input]
  (solve input 14))

(comment
  (part1 full-input)
  1538

  (part2 full-input)
  2315)


